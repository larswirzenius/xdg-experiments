use std::path::PathBuf;

const PROJECT: &str = "foo";
const ORG: &str = "org";
const APP: &str = "app";

#[derive(Debug)]
struct XdgDirs {
    cache_dir: PathBuf,
    config_dir: PathBuf,
    data_dir: PathBuf,
    runtime_dir: PathBuf,
}

fn main() {
    println!("platform-dirs: {:#?}", platform_dirs());
    println!("directories-next: {:#?}", directories_next());
}

fn platform_dirs() -> XdgDirs {
    let x = platform_dirs::AppDirs::new(Some(PROJECT), false).unwrap();
    XdgDirs {
        cache_dir: x.cache_dir,
        config_dir: x.config_dir,
        data_dir: x.data_dir,
        runtime_dir: x.state_dir,
    }
}

fn directories_next() -> XdgDirs {
    let x = directories_next::ProjectDirs::from(PROJECT, ORG, APP).unwrap();
    XdgDirs {
        cache_dir: x.cache_dir().to_path_buf(),
        config_dir: x.config_dir().to_path_buf(),
        data_dir: x.data_dir().to_path_buf(),
        runtime_dir: x.runtime_dir().unwrap().to_path_buf(),
    }
}
